#include "runahead.hh"

PhysRegFile *runahead::runahead_regfile;
UnifiedRenameMap runahead::commit_rename_map;
UnifiedRenameMap runahead::rename_map;

bool runahead::runahead_mode;

int runahead::dummy_counter;

unsigned long runahead::pc;

bool runahead::no_more_load;

std::vector<unsigned long> runahead::outstanding_miss_pc;

runahead_state runahead::state;

int runahead::runs;

QueuedSlavePort *runahead::port;

void runahead::runahead_store(Packet *pkt, Tick time){

        if(pkt->needsResponse()){
                port->schedTimingResp(pkt, time, true);
        }

}

void runahead::runahead_load(Packet *pkt, Tick time){

        char *bogus_state = new char(pkt->getSize());

        memset(bogus_state, 20, pkt->getSize());

        std::memcpy(pkt->getPtr<uint8_t>(), bogus_state, pkt->getSize());

}
