#ifndef RUNAHEAD_MODE
#define RUNAHEAD_MODE

#include "cpu/o3/regfile.hh"
#include "cpu/o3/rename_map.hh"
#include "cpu/o3/deriv.hh"
#include "mem/packet.hh"
#include "mem/qport.hh"

using namespace std;

enum runahead_state { \
        NORMAL_EXEC,
        SQUASH_INITIAL,
        SQUASH_INITIAL_TRIGGERED,
        CHECKPOINT,
        RUNAHEAD_EXEC,
        SQUASH,
        RESTORE_CONTEXT,
        RESTORE_DONE,
        SQUASH_DONE
};

class runahead{

        public:
        static PhysRegFile *runahead_regfile;
        static UnifiedRenameMap rename_map;
        static UnifiedRenameMap commit_rename_map;
        static bool runahead_mode;
        static bool no_more_load;

        static int dummy_counter;

        static unsigned long pc;

        static std::vector<unsigned long> outstanding_miss_pc;

        static runahead_state state;

        static int runs;

        static void runahead_load(Packet *pkt, Tick time);

        static void runahead_store(Packet *pkt, Tick time);

        static QueuedSlavePort *port;
};
#endif
